package com.daksh.movieinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.daksh.movieinfo.model.Movie;
import com.daksh.movieinfo.model.MovieSummary;

@RestController
@RequestMapping("/movies")
public class MoviesController {

	@Value("${api.key}")
	private String apiKey;
	
	@Autowired
	RestTemplate restTemplate;
	
	@RequestMapping("/{movieId}")
	public Movie getMovie(@PathVariable("movieId") String movieId) {
		if("1234".equals(movieId))
			return new Movie(movieId, "Don", "test");
		else
			return new Movie(movieId, "Trasformer", "best");
	}
	
	
	@RequestMapping("/api/{movieId}")
	public Movie getMovieInfo(@PathVariable("movieId") String movieId) {
		MovieSummary movieSummary = restTemplate.getForObject("https://api.themoviedb.org/3/movie/"+movieId + "?api_key="+apiKey, MovieSummary.class);
		
		return new Movie(movieSummary.getId(), movieSummary.getTitle(), movieSummary.getOverview());
	}
}
